<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2018/6/4
  Time: 17:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>

<html>
<head>
    <title>教师注册</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <meta name="keywords" content="" />
    <link href="<%=path%>/LCH/CSS/style2.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>

<div class="signupform">
    <h1>教师用户注册</h1>
    <div class="container">
        <div class="agile_info">
            <div class="w3l_form">
                <div class="left_grid_info">
                    <h3>Welcome to Zhongyuan University educational administration system ! ! </h3>
                    <h4></h4>
                </div>
            </div>
            <div class="w3_info">
                <h2>&nbsp;&nbsp;&nbsp;请填写下列信息进行注册</h2>
                <p></p>
                <form action="/registersuccess.html" method="post">
                    <div class="input-group">
                        <span><i class="fa fa-user" aria-hidden="true"></i></span>
                        <input type="text" name="user_id" placeholder=" user_id" required="">
                    </div>
                    <div class="input-group">
                        <span><i class="fa fa-user" aria-hidden="true"></i></span>
                        <input type="text"  name="name" placeholder="name" required="">
                    </div>
                    <div class="input-group"><span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                        <input type="text" name="user_name" placeholder="user_name" required="">
                    </div>
                    <div class="input-group">
                        <span><i class="fa fa-lock" aria-hidden="true"></i></span>
                        <input type="Password"  name="password" placeholder="password" required="">
                    </div>
                    <div class="input-group">
                        <span><i class="fa fa-lock" aria-hidden="true"></i></span>
                        <input type="text"  name="phonenumber"placeholder="phonenumber" required="">
                    </div>
                    <p></p>
                    <button class="btn btn-danger btn-block" type="submit">Create Account</button >
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="footer">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;出现任何疑问请联系我们@8888888
    </div>
</div>

</body>
</html>
<%--
<head>
    <title>Title</title>
</head>
<body>
<form method="post" action="/registersuccess.html">
    工号：<input name="user_id" type="text"><br>
    姓名:<input name="name" type="text"><br>
    用户名：<input name="user_name" type="text"><br>
    密码:<input name="password" type="password"><br>
    手机号:<input name="phonenumber" type="text"><br>
    <input type="submit" value="提交"><br>
</form>

</body>
</html>--%>
