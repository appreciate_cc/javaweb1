<%--
实现查看个人基本信息的页面
  Created by IntelliJ IDEA.
  User: 王倩
  Date: 2018/6/10
  Time: 20:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>查看个人信息</title>
    <script type="text/javascript">
    </script>
    <style type="text/css">
        body{
            width:100%;
            margin:0 auto;
        }
        #head{
            width:100%;
            height:100px;
            background-color: #183db2;
        }
        .header{
            width:100%;
            height:100px;
            font-size: 40px;
            font-family: 华文隶书;
            color: rgba(255, 0, 0, 0.68);
            text-align: center;
            line-height: 100px;
        }
        #menu{
            width:100%;
            height:20px;
            background-color: #e3e8bd;
        }
        #sidebar{
            width:20%;
            height:800px;
            background-color: #148e89;
            float:left;
        }
        .right{
            margin-top: 0px;
            margin-left:0px;
            z-index: 100;
            width:80%;
            height:800px;
            background-color:lightgray;
            float:left;

        }
    </style>
</head>
<body>
<div id="head">
    <div class="header">教师用户中心 </div>
</div>
<div id="menu"></div>
<div id="sidebar">
    <a href="/qian/jsp/main1.jsp">查看个人基本信息</a><p/>
    <a href="/main22.html">查看全部信息</a><p/>
    <a href="/qian/jsp/main3.jsp">修改基本信息</a><p/>
    <a href="/qian/jsp/main4.jsp">删除基本信息</a><p/>
</div>
<%--<div id="main">
</div>--%>
<div class="right">
<form action="/main12.html" method="post">
    user_id:<input name="user_id" type="text"> <br><br>
    <input type="submit" value="查询">
</form>
    <table style="width:200px;height:auto;" border="1">
        <tr style="width:200px;height:30px;" align="center">
            <td align="center" width="50px">user_id</td>
            <td align="center" width="50px">name</td>
            <td align="center" width="50px">user_name</td>
            <td align="center" width="50px">password</td>
            <td align="center" width="50px">phonenumber</td>
        </tr>
        <tr><td align="center">${ main1.user_id}</td>
            <td align="center">${ main1.name}</td>
            <td align="center">${ main1.user_name}</td>
            <td align="center">${ main1.password}</td>
            <td align="center">${ main1.phonenumber}</td>
 </tr>
    </table>

</div>

<%--<a href="javascript:history.go(-2);">返回教师用户中心</a><br />--%>
</body>
</html>