<%--
  Created by IntelliJ IDEA.
  User: 王倩
  Date: 2018/6/3
  Time: 12:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <script type="text/javascript">

    </script>
    <style type="text/css">
        body{
            width:100%;
            margin:0 auto;
        }
        #head{
            width:100%;
            height:100px;
            background-color: #183db2;
        }
        .header{
            width:100%;
            height:100px;
            font-size: 40px;
            font-family: 华文隶书;
            color: rgba(255, 0, 0, 0.68);
            text-align: center;
            line-height: 100px;
        }
        #menu{
            width:100%;
            height:20px;
            background-color: #e3e8bd;
        }
        #sidebar{
            width:20%;
            height:500px;
            background-color: #148e89;
            float:left;
        }
        #main{
            width:80%;
            height:500px;
            background-color:lightgray;
            float:left;
        }
        #foot{
            width:100%;
            height:50px;
            background-color: #16328e;
        }
    </style>
</head>
<body>
<div id="head">
    <div class="header">教师用户中心 </div>
</div>
<div id="menu"></div>
<div id="sidebar">
    <a href="/qian/jsp/main1.jsp">查看个人基本信息</a><p/>
    <a href="/main22.html">查看全部信息</a><p/>
    <a href="/qian/jsp/main3.jsp">修改基本信息</a><p/>
    <a href="/qian/jsp/main4.jsp">删除基本信息</a><p/>
</div>
<div id="main">
</div>
<div id="foot">foot</div>
</body>
</html>



