<%--
修改基本信息
  Created by IntelliJ IDEA.
  User: wangqian
  Date: 2018/6/10
  Time: 20:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style type="text/css">
        body{
            width:100%;
            margin:0 auto;
        }
        #head{
            width:100%;
            height:100px;
            background-color: #183db2;
        }
        .header{
            width:100%;
            height:100px;
            font-size: 40px;
            font-family: 华文隶书;
            color: rgba(255, 0, 0, 0.68);
            text-align: center;
            line-height: 100px;
        }
        #menu{
            width:100%;
            height:20px;
            background-color: #e3e8bd;
        }
        #sidebar{
            width:20%;
            height:800px;
            background-color: #148e89;
            float:left;
        }
        .right{
            margin-top: 0px;
            margin-left:0px;
            z-index: 100;
            width:80%;
            height:800px;
            background-color:lightgray;
            float:left;

        }
    </style>
</head>
<body>
<div id="head">
    <div class="header">教师用户中心 </div>
</div>
<div id="menu"></div>
<div id="sidebar">
    <a href="/qian/jsp/main1.jsp">查看个人基本信息</a><p/>
    <a href="/main22.html">查看全部信息</a><p/>
    <a href="/qian/jsp/main3.jsp">修改基本信息</a><p/>
    <a href="/qian/jsp/main4.jsp">删除基本信息</a><p/>
</div>
<div class="right">
<form method="post" action="/main32.html">
    <table>
        <tr><td>工号:</td><td><input name="user_id" type="text"></td></tr>
        <tr><td>姓名:</td><td><input name="name" type="text"></td></tr>
        <tr><td>用户名:</td><td><input name="user_name" type="text"></td></tr>
        <tr><td>密码:</td><td><input name="password" type="password"></td></tr>
        <tr><td>手机号:</td><td><input name="phonenumber" type="text"></td></tr>

        <tr><td colspan="2" align="center"><input type="submit" value="修改信息"></td></tr>
    </table>
</form>
    请点 查看全部信息 查看修改情况！
</div>
</body>
</html>

