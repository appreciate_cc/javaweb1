package com.smartcc.service;

import com.smartcc.dao.SUserDao;
import com.smartcc.daomain.entity.student_user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class studentservice {
    private SUserDao sUserDao;
    @Autowired
    private com.smartcc.daomain.impl.student_userDao student_userDao;
    @Autowired
    public void setUserDao(SUserDao userDao) {
        this.sUserDao = userDao;
    }

    public boolean hasMatchUser(String userName, String password) {
        int matchCount = sUserDao.getMatchCount(userName, password);
        return matchCount > 0;
    }
    public student_user findUserByUserName(String userName) {
        return sUserDao.findUserByUserName(userName);
    }

    public void insert(student_user stu_user) { student_userDao.insert(stu_user);}
    public void update(student_user student_user) {student_userDao.update(student_user);}
}

