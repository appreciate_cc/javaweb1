package com.smartcc.web;

import com.smartcc.daomain.entity.student_user;
import com.smartcc.daomain.impl.student_userDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;


@RestController
public class SRegisiterController {
    @Autowired
    private student_userDao student_userDao;

    @RequestMapping(value = "/insert.html") //截获index.html访问，重定向到add.jsp
    public String index_insert(student_user suser) {
        System.out.println(suser);
        if(suser.getStudent_id().equals("")||suser.getStudent_name().equals("")||suser.getStudent_phonenum().equals("")
                ||suser.getStudent_username().equals("")||suser.getStudent_userpwd().equals(""))
         return "cc";
        student_userDao.insert(suser);
        return "registersucessful";
    }
    @RequestMapping(value = "/delete.html")
    public String index_delete(String student_id){
        student_userDao.delete(student_id);
        return "cc";
    }
    @RequestMapping(value = "/update.html")
    public String index_update(student_user student_user) {
        student_userDao.update(student_user);
        return "cc";
    }
    @RequestMapping(value = "/cs.html")
    public String index_fanhui(){
        return "cc";
    }
    @RequestMapping(value = "/queryById.html")
    public ModelAndView index_queryById(String student_id) throws SQLException {
        student_user stuuser = null;
        stuuser =student_userDao.queryById(student_id);
        ModelAndView mav = new ModelAndView();
        mav.addObject("cc", stuuser);
        mav.setViewName("cc");
        return mav;

    }
    @Autowired

    public void setStudent_userDao(com.smartcc.daomain.impl.student_userDao student_userDao) {
        this.student_userDao = student_userDao;
    }
}
