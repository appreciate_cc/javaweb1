package com.smartcc.dao;

import com.smartcc.daomain.entity.student_user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class SUserDao {
    private JdbcTemplate jdbcTemplate;
    @Autowired

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    private  final static String MATCH_COUNT_SQL = " SELECT count(*) FROM t_teacher  " +
            " WHERE user_name =? and password=? ";


    public int getMatchCount(String userName, String password) {

        return jdbcTemplate.queryForObject(MATCH_COUNT_SQL, new Object[]{userName, password}, Integer.class);
    }
    public student_user findUserByUserName(final String userName) {
        String sqlStr = "select user_id,name,user_name,password,phonenumber" +
                " from t_teacher where user_name=?";
        final  student_user user = new student_user();
        jdbcTemplate.query(sqlStr, new Object[] { userName },
                new RowCallbackHandler() {
                    public void processRow(ResultSet rs) throws SQLException {
                        user.setStudent_id(rs.getString("student_id"));
                        user.setStudent_name(rs.getString("student_name"));
                        user.setStudent_username(rs.getString("student_username"));
                        user.setStudent_userpwd(rs.getString("student_userpwd"));
                        user.setStudent_phonenum(rs.getString("student_phonenum"));
                    }
                });
        return user;
    }
}
