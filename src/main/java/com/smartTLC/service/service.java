package com.smartTLC.service;

import com.smartTLC.domain.entity.OtherUser;
import com.smartTLC.domain.impl.OtherUserDao;

import java.util.List;

public class service {

    private OtherUserDao otherUserDao;

    public service(OtherUserDao otherUserDao) {
        this.otherUserDao = otherUserDao;
    }

    public void insert(OtherUser user) {
        otherUserDao.insert(user);
    }
    public OtherUser querybyid(String id){
        return otherUserDao.queryById(id);
    }
    public List queryall(){
        return otherUserDao.queryALL();
    }
    public void deleteteacher(
            String id){otherUserDao.delete(id);
    }
    public void Update(
            OtherUser user){otherUserDao.update(user);
    }

}
