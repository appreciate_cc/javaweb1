package com.smartTLC.domain.entity;

import java.util.List;

public  class OtherUser {
    private String id;
    private String name;
    private String username;
    private String userpwd;
    private String phonenum;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpwd() {
        return userpwd;
    }

    public void setUserpwd(String userpwd) {
        this.userpwd = userpwd;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    @Override
    public String toString() {
        return "OtherUser{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", userpwd='" + userpwd + '\'' +
                ", phonenum='" + phonenum + '\'' +
                '}';
    }

    public void update(OtherUser user) {

    }

    public void delete(String id) {

    }

    public void insert(OtherUser user) {

    }

    public OtherUser queryById(String id){
        return null;
    }

    public List queryALL() {
        return null;
    }
}
