package com.smartTLC.domain.impl;
import com.smartTLC.domain.entity.OtherUser;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

public interface OtherUserDao {
    public void update(OtherUser user);
    public void delete(String id);
    public void insert(OtherUser user);
    public OtherUser queryById(String id);
    public List queryALL();
}
