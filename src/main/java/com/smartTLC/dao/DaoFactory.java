package com.smartTLC.dao;

import com.smartTLC.domain.entity.OtherUser;
import com.smartTLC.domain.impl.OtherUserDao;
import com.smartTLC.domain.impl.OtherUserDaompl;

import java.util.List;

public class DaoFactory {
    public static class DAOFactory {
        public static OtherUserDao getOtherUserDaoInstance() {
            return new OtherUserDaompl(){
                @Override
                public void update(OtherUser user) {

                }

                @Override
                public void delete(String id) {

                }

                @Override
                public void insert(OtherUser guanli_user) {

                }

                @Override
                public OtherUser queryById(String id){
                    return null;
                }

                @Override
                public List queryALL() {
                    return null;
                }

            };
        }
    }

}
