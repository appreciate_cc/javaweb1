package com.smartwmx.daomain.entity;

public class guanli_user {
    private String guanli_id;
    private String guanli_name;
    private String guanli_username;
    private String guanli_userpwd;
    private String guanli_phonenum;

    public String getGuanli_id() {
        return guanli_id;
    }

    public void setGuanli_id(String guanli_id) {
        this.guanli_id = guanli_id;
    }

    public String getGuanli_name() {
        return guanli_name;
    }

    public void setGuanli_name(String guanli_name) {
        this.guanli_name = guanli_name;
    }

    public String getGuanli_username() {
        return guanli_username;
    }

    public void setGuanli_username(String guanli_username) {
        this.guanli_username = guanli_username;
    }

    public String getGuanli_userpwd() {
        return guanli_userpwd;
    }

    public void setGuanli_userpwd(String guanli_userpwd) {
        this.guanli_userpwd = guanli_userpwd;
    }

    public String getGuanli_phonenum() {
        return guanli_phonenum;
    }

    public void setGuanli_phonenum(String guanli_phonenum) {
        this.guanli_phonenum = guanli_phonenum;
    }

    @Override
    public String toString() {
        return "guanli_user{" +
                "guanli_id=" + guanli_id +
                ", guanli_name='" + guanli_name + '\'' +
                ", guanli_username='" + guanli_username + '\'' +
                ", guanli_userpwd='" + guanli_userpwd + '\'' +
                ", guanli_phonenum='" + guanli_phonenum + '\'' +
                '}';
    }
}

