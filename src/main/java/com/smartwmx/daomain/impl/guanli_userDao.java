package com.smartwmx.daomain.impl;

import com.smartwmx.daomain.entity.guanli_user;

import java.sql.SQLException;
import java.util.List;

public interface guanli_userDao {
    public void update(guanli_user guanli_user);
    public void delete(String guanli_id);
    public void insert(guanli_user guanli_user);
    public guanli_user queryById(String guanli_id) throws SQLException;
    public List queryALL();
}
