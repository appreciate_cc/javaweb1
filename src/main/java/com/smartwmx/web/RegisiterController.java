package com.smartwmx.web;

import com.smartwmx.daomain.entity.guanli_user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class RegisiterController {
    @Autowired
    private com.smartwmx.service.guanliservice guanliservice;

    @RequestMapping(value = "/register.html") //截获index.html访问，重定向到add.jsp
    public String index() {
        return "guanli";
    }
    @RequestMapping(value = "/registersuccess.html")
    public String addtuser(HttpServletRequest request,guanli_user gua_user) {
        System.out.println("管理员："+gua_user);
        System.out.println(guanliservice);
        if(gua_user.getGuanli_id()==""||gua_user.getGuanli_name()==""||gua_user.getGuanli_username()==""
                ||gua_user.getGuanli_userpwd()==""||gua_user.getGuanli_phonenum()=="")
            return "guanli";
        guanliservice.insert(gua_user);
        return "registersuccess";
    }
    @Autowired
    public void setGuanliservice(com.smartwmx.service.guanliservice guauserService)
    {
        this.guanliservice =guauserService;
    }
}
