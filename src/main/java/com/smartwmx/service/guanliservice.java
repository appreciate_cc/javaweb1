package com.smartwmx.service;

import com.smartwmx.dao.GUserDao;
import com.smartwmx.daomain.entity.guanli_user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class guanliservice {
    private GUserDao gUserDao;
    @Autowired
    private com.smartwmx.daomain.impl.guanli_userDao guanli_userDao;
    @Autowired
    public void setUserDao(GUserDao userDao) {
        this.gUserDao = userDao;
    }

    public boolean hasMatchUser(String userName, String password) {
        int matchCount = gUserDao.getMatchCount(userName, password);
        return matchCount > 0;
    }
    public guanli_user findUserByUserName(String userName) {
        return gUserDao.findUserByUserName(userName);
    }

    public void insert(guanli_user gua_user) { guanli_userDao.insert(gua_user);}
    public void update(guanli_user guanli_user) {guanli_userDao.update(guanli_user);}
}
