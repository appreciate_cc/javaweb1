package com.smartwmx.dao;

import com.smartwmx.daomain.entity.guanli_user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class GUserDao {
    private JdbcTemplate jdbcTemplate;
    @Autowired

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    private  final static String MATCH_COUNT_SQL = " SELECT count(*) FROM t_teacher  " +
            " WHERE user_name =? and password=? ";


    public int getMatchCount(String userName, String password) {

        return jdbcTemplate.queryForObject(MATCH_COUNT_SQL, new Object[]{userName, password}, Integer.class);
    }
    public guanli_user findUserByUserName(final String userName) {
        String sqlStr = "select user_id,name,user_name,password,phonenumber" +
                " from t_teacher where user_name=?";
        final guanli_user user = new guanli_user();
        jdbcTemplate.query(sqlStr, new Object[] { userName },
                new RowCallbackHandler() {
                    public void processRow(ResultSet rs) throws SQLException {
                        user.setGuanli_id(rs.getString("guanli_id"));
                        user.setGuanli_name(rs.getString("guanli_name"));
                        user.setGuanli_username(rs.getString("guanli_username"));
                        user.setGuanli_userpwd(rs.getString("guanli_userpwd"));
                        user.setGuanli_phonenum(rs.getString("guanli_phonenum"));
                    }
                });
        return user;
    }
}
