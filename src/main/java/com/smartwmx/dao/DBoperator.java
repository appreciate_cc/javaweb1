package com.smartwmx.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBoperator {
    final String DRIVER = "com.mysql.jdbc.Driver";
    final String USER = "root";
    final String PWD = "zx175634";
    final String URL = "jdbc:mysql://localhost:3309/javaee";
    private Connection conn = null;
    public DBoperator() {
        try {
            Class.forName(DRIVER);
            this.conn = DriverManager.getConnection(URL, USER, PWD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return conn;
    }

    public void close() {
        try {
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
