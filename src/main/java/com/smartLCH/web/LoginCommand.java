package com.smartLCH.web;

public class LoginCommand {
    private  String  name;
    private  String p;

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasMatchUser(String name, String p) {
        return false;
    }

    public int findUserByUserName(String name) {
        return 0;
    }
}
