package com.smartLCH.domain.impl;

import com.smart.dao.DBoperator;
import com.smartLCH.domain.entity.manageruser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class manageruserDaolmpl implements manageruserDao {
    private PreparedStatement pstmt;

    @Override
    public void insert(manageruser manageruser) {
        String sql="insert into m_manager(user_id, name, number, phone, p) VALUES (?,?,?,?,?)";
        PreparedStatement p= null;
        DBoperator dbc = null;
        try {
            dbc = new DBoperator();
            pstmt = dbc.getConnection().prepareStatement(sql);
            pstmt.setString(1,manageruser.getUser_id());
            pstmt.setString(2,manageruser.getName());
            pstmt.setString(3,manageruser.getNumber());
            pstmt.setString(4,manageruser.getPhone());
            pstmt.setString(5,manageruser.getP());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbc.close();
        }
    }

    @Override
    public void update(manageruser manageruser) {
        String sql="update m_manager set user_id=?,name=?,number=?,phone=?,p=? ";
        PreparedStatement pstmt = null;
        DBoperator dbc = null;
        try {
            dbc = new DBoperator();
            pstmt = dbc.getConnection().prepareStatement(sql);
            pstmt.setString(1,manageruser.getUser_id());
            pstmt.setString(2,manageruser.getName());
            pstmt.setString(3,manageruser.getNumber());
            pstmt.setString(4,manageruser.getPhone());
            pstmt.setString(5,manageruser.getP());
            pstmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbc.close();
        }
    }

    @Override
    public void delete(String userid)  {
        String sql="delete from m_manager where user_id=?";
        PreparedStatement pstmt = null;
        DBoperator dbc = null;
        try {
            dbc = new DBoperator();
            pstmt = dbc.getConnection().prepareStatement(sql);
            pstmt.setString(1,userid);
            pstmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbc.close();
        }
    }

    @Override
    public manageruser queryById(String user_id) {
        String sql="select user_id,name,number,phone,p" +
                " from m_manager where user_id=?";
        PreparedStatement pstmt = null;
        DBoperator dbc = null;
        try {
            dbc = new DBoperator();
            pstmt = dbc.getConnection().prepareStatement(sql);
            pstmt.setString(1,user_id);
            ResultSet rs=pstmt.executeQuery();
            if(rs.next()){
                String name=rs.getString(2);
                String number=rs.getString(3);
                String phone=rs.getString(4);
                String p=rs.getString(5);
                manageruser mauser=new manageruser();
                mauser.setUser_id(user_id);
                mauser.setName(name);
                mauser.setNumber(number);
                mauser.setPhone(phone);
                mauser.setP(p);
                return mauser;
            }
            pstmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbc.close();
        }
        return null;
    }

    @Override
    public List queryAll() {
        String sql="select * from m_manager ";
        Statement stmt = null;
        DBoperator dbc = null;
        try {
            dbc = new DBoperator();
            stmt = dbc.getConnection().createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            List list=new ArrayList();
            while(rs.next()){
                String user_id=rs.getString(1);
                String name=rs.getString(2);
                String number=rs.getString(3);
                String phone=rs.getString(4);
                String p=rs.getString(5);
                manageruser mauser=new manageruser();
                mauser.setUser_id(user_id);
                mauser.setName(name);
                mauser.setNumber(number);
                mauser.setPhone(phone);
                mauser.setP(p);
                list.add(mauser);

            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbc.close();
        }
        return null;
    }


}
