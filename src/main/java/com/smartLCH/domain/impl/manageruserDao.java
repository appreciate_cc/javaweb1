package com.smartLCH.domain.impl;

import com.smartLCH.domain.entity.manageruser;

import java.util.List;

public interface manageruserDao {
    public void insert(manageruser manageruser);
    public void update(manageruser manageruser);
    public void delete(String user_id);
    public manageruser queryById(String user_id);
    public List queryAll();
}
