package com.smartLCH.domain;

import com.smartLCH.domain.entity.manageruser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class UserDao {
    private JdbcTemplate jdbcTemplate;

    @Autowired

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    private  final static String MATCH_COUNT_SQL = " SELECT count(*) FROM m_manager  " +
            " WHERE name =? and password=? ";


    public int getMatchCount(String Name, String p) {

        Object passward = new Object();
        return jdbcTemplate.queryForObject(MATCH_COUNT_SQL, new Object[]{Name, p},Integer.class);
    }
    public manageruser findUserByName(final String Name) {
        String sqlStr = "select user_id,name,number,phone,p" +
                " from m_manager where name=?";
        final  manageruser user = new  manageruser();
        jdbcTemplate.query(sqlStr, new Object[] { Name },
                new RowCallbackHandler() {
                    public void processRow(ResultSet rs) throws SQLException {
                        user.setUser_id(rs.getString("user_id"));
                        user.setName(rs.getString("name"));
                        user.setNumber(rs.getString("number"));
                        user.setPhone(rs.getString("phone"));
                        user.setP(rs.getString("p"));
                    }
                });
        return user;
    }


}
