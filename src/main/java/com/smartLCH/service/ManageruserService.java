package com.smartLCH.service;

import com.smart.domain.UserDao;
import com.smartLCH.domain.entity.manageruser;
import com.smartLCH.domain.impl.manageruserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManageruserService {
    private UserDao userDao;
    @Autowired
    private manageruserDao mDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public boolean hasMatchUser(String Name, String p) {
        int matchCount =userDao.getMatchCount( Name,p);
        return matchCount>0;
    }

    public void insert(manageruser manager) { mDao.insert(manager);}


    public static ManageruserService findUserByUserName(String name) {
        return null;
    }

    public manageruser querybyid(String user_id) {
        return mDao.queryById(user_id);
    }
    public List queryall(){return mDao.queryAll();}
    public void deletemanager(String user_id){mDao.delete(user_id);}
    public void Update(manageruser tea){mDao.update(tea);}
}
