package com.smart.service;

import com.smart.domain.entity.Teacher;
import com.smart.domain.impl.TeacheruserDao;
import com.smart.domain.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TeacheruserService 负责将UserDao等组织起来 完成用户名/密码认证等操作
 */
@Service
public class TeacheruserService {
    private UserDao userDao;
    @Autowired
    private TeacheruserDao teacheruserDao;
    @Autowired

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public boolean hasMatchUser(String userName, String password) {
        int matchCount =userDao.getMatchCount( userName,password);
        return matchCount>0;
    }


    public Teacher findUserByUserName(String userName) {
        return userDao.findUserByUserName(userName);
    }

    public void insert(Teacher teacher) { teacheruserDao.insert(teacher);}

    public Teacher querybyid(String user_id){return teacheruserDao.queryById(user_id);}
    public List queryall(){return teacheruserDao.queryAll();}
    public void deleteteacher(String user_id){teacheruserDao.delete(user_id);}
    public void Update(Teacher tea){teacheruserDao.update(tea);}

}
