package com.smart.domain.impl;
import com.smart.domain.entity.Teacher;

import java.util.List;

public interface TeacheruserDao {
    public void insert(Teacher teacheruser);
    public void update(Teacher teacheruser);
    public void delete(String userid);
    public  Teacher queryById(String userid);
    public List queryAll();

}
