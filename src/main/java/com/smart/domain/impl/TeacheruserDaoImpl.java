package com.smart.domain.impl;

import com.smart.dao.DBoperator;
import com.smart.domain.entity.Teacher;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TeacheruserDaoImpl implements TeacheruserDao {
    @Override
    public void insert( Teacher teacheruser) {
        String sql="insert into t_teacher(user_id, name, user_name, password, phonenumber) VALUES (?,?,?,?,?)";
        PreparedStatement pstmt = null;
        DBoperator dbc = null;
        try {
            dbc = new DBoperator();
            pstmt = dbc.getConnection().prepareStatement(sql);
            pstmt.setString(1,teacheruser.getUser_id());
            pstmt.setString(2,teacheruser.getName());
            pstmt.setString(3,teacheruser.getUser_name());
            pstmt.setString(4,teacheruser.getPassword());
            pstmt.setString(5,teacheruser.getPhonenumber());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbc.close();
        }
    }

    @Override
    public void update( Teacher teacheruser) {
        String sql="update t_teacher set user_id=?,name=?,user_name=?,password=?,phonenumber=? where user_id=?";
        PreparedStatement pstmt = null;
        DBoperator dbc = null;
        try {
                dbc = new DBoperator();
                pstmt = dbc.getConnection().prepareStatement(sql);
                pstmt.setString(1,teacheruser.getUser_id());
                pstmt.setString(2,teacheruser.getName());
                pstmt.setString(3,teacheruser.getUser_name());
                pstmt.setString(4,teacheruser.getPassword());
                pstmt.setString(5,teacheruser.getPhonenumber());
                pstmt.setString(6, teacheruser.getUser_id());
                pstmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbc.close();
        }
    }

    @Override
    public void delete(String userid)  {
        String sql="delete from t_teacher where user_id=?";
        PreparedStatement pstmt = null;
        DBoperator dbc = null;
        try {
            dbc = new DBoperator();
            pstmt = dbc.getConnection().prepareStatement(sql);
            pstmt.setString(1,userid);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbc.close();
        }
    }

    @Override
    public Teacher queryById(String userid) {
        String sql="select user_id,name,user_name,password,phonenumber" +
                " from t_teacher where user_id=?";
        PreparedStatement pstmt = null;
        DBoperator dbc = null;
        try {
            dbc = new DBoperator();
            pstmt = dbc.getConnection().prepareStatement(sql);
            pstmt.setString(1,userid);
            ResultSet rs=pstmt.executeQuery();
            if(rs.next()){
                String names=rs.getString(2);
                String usernames=rs.getString(3);
                String pwd=rs.getString(4);
                String num=rs.getString(5);
                Teacher teuser=new   Teacher();
                teuser.setUser_id(userid);
                teuser.setName(names);
                teuser.setUser_name(usernames);
                teuser.setPassword(pwd);
                teuser.setPhonenumber(num);
                return teuser;
            }
            pstmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbc.close();
        }
        return null;
    }

    @Override
    public List queryAll() {
        String sql="select * from t_teacher ";
        Statement stmt = null;
        DBoperator dbc = null;
        try {
            dbc = new DBoperator();
            stmt = dbc.getConnection().createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            List list=new ArrayList();
            while(rs.next()){
                String id=rs.getString(1);
                String names=rs.getString(2);
                String usernames=rs.getString(3);
                String pwd=rs.getString(4);
                String num=rs.getString(5);
                Teacher teuser=new Teacher();
                teuser.setUser_id(id);
                teuser.setName(names);
                teuser.setUser_name(usernames);
                teuser.setPassword(pwd);
                teuser.setPhonenumber(num);
                list.add(teuser);

            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbc.close();
        }
        return null;
    }
}