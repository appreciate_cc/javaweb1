package com.smart.domain;

import com.smart.domain.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;

import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
@Repository
public class UserDao {
    private JdbcTemplate jdbcTemplate;
    @Autowired

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    private  final static String MATCH_COUNT_SQL = " SELECT count(*) FROM t_teacher  " +
            " WHERE user_name =? and password=? ";


    public int getMatchCount(String userName, String password) {

        return jdbcTemplate.queryForObject(MATCH_COUNT_SQL, new Object[]{userName, password}, Integer.class);
    }


    public Teacher findUserByUserName(final String userName) {
        String sqlStr = "select user_id,name,user_name,password,phonenumber" +
                " from t_teacher where user_name=?";
        final  Teacher user = new  Teacher();
        jdbcTemplate.query(sqlStr, new Object[] { userName },
                new RowCallbackHandler() {
                    public void processRow(ResultSet rs) throws SQLException {
                        user.setUser_id(rs.getString("user_id"));
                        user.setName(rs.getString("name"));
                        user.setUser_name(userName);
                        user.setPassword(rs.getString("password"));
                        user.setPhonenumber(rs.getString("phonenumber"));
                    }
                });
        return user;
    }

}
