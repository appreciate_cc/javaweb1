package com.smart.web;

import com.smart.domain.entity.Teacher;
import com.smart.service.TeacheruserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 教师注册页面 注册成功到registersucces.jsp 否则留在当前页
 */
@Controller
public class RegisiterController {
    @Autowired
   private TeacheruserService teacheruserService;

    @RequestMapping(value = "/register.html")
    public String index() {
        return "teacher";
    }
  @RequestMapping(value = "/registersuccess.html")
    public String addtuser(HttpServletRequest request,Teacher tuser) {
      System.out.println("教师信息："+tuser);
      System.out.println(teacheruserService);
      String types=request.getParameter("sf");

        if(tuser.getUser_id().equals("")||tuser.getName().equals("")||tuser.getUser_name().equals("")
                ||tuser.getPassword().equals("")||tuser.getPhonenumber().equals(""))
            return "teacher";
              teacheruserService.insert(tuser);
        return "registersuccess";
    }
    @Autowired
    public void setTeacheruserService(TeacheruserService teacheruserService) {
        this.teacheruserService = teacheruserService;
    }
}
