package com.smart.web;

import com.smart.domain.entity.Teacher;
import com.smart.service.TeacheruserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author wangqian
 * @Descript 实现在教师用户中心页面 点击修改信息 的功能
 */
@Controller
public class UpdateController {
    @Autowired
TeacheruserService teacheruserService;
    @RequestMapping(value = "/main3.html")
    public String index() {
        return "main3";
    }
    @RequestMapping(value = "/main32.html")
    public ModelAndView updateteacher(HttpServletRequest request, Teacher tea){
        teacheruserService.Update(tea);
        System.out.println(tea);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("main3", tea);
        modelAndView.setViewName("main3");
        return modelAndView;

    }

}
