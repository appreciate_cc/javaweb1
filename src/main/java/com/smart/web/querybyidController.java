package com.smart.web;

import com.smart.domain.entity.Teacher;
import com.smart.service.TeacheruserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;

/**
 * @author  wangqian
 * @descript 实现在教师用户中心页查看个人基本信息功能
 */

@Controller
public class querybyidController {
    @Autowired
    TeacheruserService teacheruserService;
    @RequestMapping(value = "/main1.html")
    public String index() {
        return "main1";
    }
    @RequestMapping(value = "/main12.html")
        public ModelAndView Qurybyid(HttpServletRequest request, String user_id){

       Teacher tea=teacheruserService.querybyid(user_id);
       System.out.println(tea);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("main1", tea);
            modelAndView.setViewName("main1");
            return modelAndView;
        }

    @Autowired
    public void setTeacheruserService(TeacheruserService teacheruserService) {
        this.teacheruserService = teacheruserService;
    }

}
