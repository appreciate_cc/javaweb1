package com.smart.web;
import com.smart.domain.entity.Teacher;
import com.smart.service.TeacheruserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 完成输入用户名密码之后页面的跳转 数据库中若可以查找到该信息 就登陆进入教师用户中心
 * 若数据库中没有该用户 就进入注册页面 完成注册
 */
@RestController
public class LoginController {
    private TeacheruserService teacheruserService;
    @RequestMapping(value = "/index.html")//截获index.html访问，重定向到gg.jsp
    public String indexPage(){
        return "gg";
    }
    @RequestMapping(value = "/LoginCheck.html")
    public ModelAndView LoginCheck(HttpServletRequest request,Teacher tuser)throws Exception{
        String types=request.getParameter("sf");
        boolean isValidUser= teacheruserService.hasMatchUser(tuser.getUser_name(),tuser.getPassword());
        if(!isValidUser){
            return new ModelAndView("fail","error","用户名或密码不存在");
            //参数为空时，跳向注册页面
        }
        else{
            Teacher us= teacheruserService.findUserByUserName(tuser.getUser_name());
            //在这里进行判断   从前台传过来的身份是啥   如果是老师跳转到一个页面   学生跳转到另一个页面
           if(types.equals("teacher")){
            request.getSession().setAttribute("user", us);
            return new ModelAndView("success");}
            else if(types.equals("student"))
               return new ModelAndView("cc");
           else
               return new ModelAndView("fail");
        }
    }
    @Autowired
    public void setUserService(TeacheruserService userService) {
        this.teacheruserService = userService;
    }
}
