package com.smart.web;

import com.smart.domain.entity.Teacher;
import com.smart.service.TeacheruserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author wangqian
 * @Descript 实现在教师用户中心页面 点击删除信息的功能
 */
@Controller
public class DeleteController {
    @Autowired
    TeacheruserService teacheruserService;
    @RequestMapping(value = "/main4.html")
    public String index() {
        return "main4";
    }
    @RequestMapping(value = "/main42.html")
    public ModelAndView Delete(HttpServletRequest request, String user_id){

        teacheruserService.deleteteacher(user_id);
        ModelAndView modelAndView = new ModelAndView();
       /* modelAndView.addObject("main2", teacherlist);*/
        modelAndView.setViewName("main4");
        return modelAndView;
}
    @Autowired
    public void setTeacheruserService(TeacheruserService teacheruserService) {
        this.teacheruserService = teacheruserService;
    }
}
