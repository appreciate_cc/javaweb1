package com.smart.web;

import com.smart.service.TeacheruserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Iterator;
import java.util.List;

/**
 * @Author wangqian
 * @Descript 实现在教师用户中心页面 点击查看全部信息 并将结果返回在页面
 */
@Controller
public class QuryallController {
    @Autowired
    TeacheruserService teacheruserService;
    @RequestMapping(value = "/main2.html")
    public String index() {
        return "main2";
    }
    @RequestMapping(value = "/main22.html")
    public ModelAndView Quryall(HttpServletRequest request){

         List teacherlist=teacheruserService.queryall();

         Iterator iter=teacherlist.iterator();
         while(iter.hasNext())
            System.out.println(iter.next().toString());

         ModelAndView modelAndView = new ModelAndView();
         modelAndView.addObject("main2", teacherlist);
         modelAndView.setViewName("main2");
        return modelAndView;
    }

    @Autowired
    public void setTeacheruserService(TeacheruserService teacheruserService) {
        this.teacheruserService = teacheruserService;
    }
}
