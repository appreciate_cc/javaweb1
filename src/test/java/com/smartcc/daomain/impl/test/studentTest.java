package com.smartcc.daomain.impl.test;

import com.smartcc.daomain.entity.student_user;
import com.smartcc.daomain.impl.student_userDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:smart-context.xml")
public class studentTest {
    @Autowired
    private student_userDao student_userDao;
    @Test
    public void insertTest(){
        student_user student_user=new student_user();
        student_user.setStudent_id("111");
        student_user.setStudent_name("sss");
        student_user.setStudent_username("admin");
        student_user.setStudent_userpwd("123456");
        student_user.setStudent_phonenum("123456789");
        student_userDao.insert(student_user);
    }
    @Test
    public void updateTest(){
        student_user student_user =new student_user();
        student_user.setStudent_id("111");
        student_user.setStudent_name("ssds");
        student_user.setStudent_username("ororo");
        student_user.setStudent_userpwd("skvkg");
        student_user.setStudent_phonenum("111166565");
        student_userDao.update(student_user);
    }
    @Test
    public void deleteTest() {
        {
            student_userDao.delete("111");
        }
    }
    @Test
    public void queryById() throws SQLException {

        System.out.println(student_userDao.queryById("156787887").toString());
    }
}
