package com.smartcc.daomain.impl.test;

import com.smartTLC.domain.entity.OtherUser;
import com.smartTLC.domain.impl.OtherUserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath*:smart-context.xml")
public class OtherUserTest {
    @Autowired
    private OtherUserDao OtherUserDao;


    @Test
    public void insertTest(){
        OtherUser otherUser=new OtherUser();
        otherUser.setId("yyy");
        otherUser.setName("abc");
        otherUser.setUsername("admin");
        otherUser.setUsername("jayyy");
        otherUser.setPhonenum("999999");
        otherUser.insert(otherUser);
    }
    @Test
    public void updateTest(){
        OtherUser otherUser =new OtherUser();
        otherUser.setId("henshuai");
        otherUser.setName("jay");
        otherUser.setUsername("gd");
        otherUser.setUserpwd("abc");
        otherUser.setPhonenum("10000");
        otherUser.update(otherUser);
    }
    @Test
    public void deleteTest() {
        {
           OtherUserDao.delete("henshuai");
        }
    }
    @Test
    public void queryById() throws SQLException {

        System.out.println(OtherUserDao.queryById("999999999").toString());
    }
}
