import com.smart.domain.entity.Teacher;
import com.smart.domain.impl.DAOFactory;
        import com.smart.domain.impl.TeacheruserDao;
import com.smart.service.TeacheruserService;

import java.util.Iterator;
        import java.util.List;

public class Test {
    public static void main(String...args) {
        //测试向t_teacher表中增加一条数据
        TeacheruserDao dao= DAOFactory.getTeacherDaoInstance();
        Teacher teacheruser=new Teacher();
        teacheruser.setUser_id("0");
        teacheruser.setName("0");
        teacheruser.setUser_name("0");
        teacheruser.setPassword("0");
        teacheruser.setPhonenumber("0");
        dao.insert(teacheruser);
        //更新t_teacher表中数据
        dao.update(teacheruser);
        //删除t_teacher表中数据
        String userid="33333";
        dao.delete(userid);
        //查找t_teacher表中数据 按user_id查找
        String id="201608020202";
        System.out.println(id+" "+dao.queryById(id).getName()+" "+dao.queryById(id).getUser_name()
        +" "+dao.queryById(id).getPassword()+" "+dao.queryById(id).getPhonenumber());
       //遍历出t_teacher表中所有用户信息
        List li=dao.queryAll();
        Iterator iter=li.iterator();
        while(iter.hasNext())
            System.out.println(iter.next().toString());
        //判断用户是否存在
        TeacheruserService teacheruserService=new TeacheruserService();
        boolean isValidUser=teacheruserService.hasMatchUser("201608020202","201608020202");
        if(isValidUser)
        System.out.println("用户存在");
        else
            System.out.println("用户不存在");

    }

}
