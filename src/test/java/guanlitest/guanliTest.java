package guanlitest;

import com.smartwmx.daomain.entity.guanli_user;
import com.smartwmx.daomain.impl.guanli_userDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:smart-context.xml")
public class guanliTest {
    @Autowired
    private guanli_userDao guanli_userDao;
    @Test
    public void insertTest(){
        guanli_user guanli_user=new guanli_user();
        guanli_user.setGuanli_id("1234");
        guanli_user.setGuanli_name("asdz");
        guanli_user.setGuanli_username("zdsa");
        guanli_user.setGuanli_userpwd("1234");
        guanli_user.setGuanli_phonenum("1234567890");
        guanli_userDao.insert(guanli_user);
    }
    /*@Test
    public void updateTest(){
        guanli_user guanli_user =new guanli_user();
        guanli_user.setGuanli_id("234");
        guanli_user.setGuanli_name("azsx");
        guanli_user.setGuanli_username("xsza");
        guanli_user.setGuanli_userpwd("2143");
        guanli_user.setGuanli_phonenum("1523687531");
        guanli_userDao.update(guanli_user);
    }*/
    /*Test
    public void deleteTest(){
        guanli_userDao.delete("234");
    }
    @Test
    public void queryById() throws SQLException {

        System.out.println(guanli_userDao.queryById("234").toString());
    }*/
}
